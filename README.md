# Credit card checkout form page with React.js
## Introduction
This project implements the daily UI design of a credit card checkout form page with react.js.
The original design can be found [here](https://dribbble.com/shots/3919953-Daily-UI-002-Credit-Card-Checkout)


## Libraries and APIs
| Library/ API     | Version    | 
| --------|---------|
| EcmaScript | 6 |
| bootstrap | 4.1.2 |
| enzyme | 3.3.0 |
| react | 16.4.1 |
| jest | 20.0.4 |
| fontawesome | 5.1.0 |

## Instructions

- Clone the Git repository:

```
git clone https://Lezlee@bitbucket.org/Lezlee/credit-card-checkout.git credit-card-checkout && cd credit-card-checkout
```

- Create and checkout a `credit-card-checkout` branch:

```
git checkout -b credit-card-checkout
```
- Install dependencies

```
npm install
```
- Run the application 
```
npm run start
```
- Run unit tests with Jest and Enzyme
```
npm run test
```
