import React from 'react';
import { Footer } from './footer';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';


describe('Footer Component', () => {
    test('should render correctly', () => {
        const footer = shallow(<Footer />);
        expect(shallowToJson(footer)).toMatchSnapshot()
    });
});

