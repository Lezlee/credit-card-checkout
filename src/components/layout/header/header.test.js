import React from 'react'
import { Header } from './header'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'


describe('Header Component', () => {
    test('should render correctly', () => {
        const header = shallow(<Header />);
        expect(shallowToJson(header)).toMatchSnapshot()
    })
})