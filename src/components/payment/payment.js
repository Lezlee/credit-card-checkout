import React from 'react';
import './payment.scss';

class Payment extends React.Component {
    handleSubmit(e) {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        let form = e.currentTarget.form;
        // Prevent submission if form is invalid
        if (form.checkValidity() === false) {
            e.preventDefault();
            e.stopPropagation();
            form.classList.add('was-validated');
        } else {
            // Make server-side API calls
            form.classList.remove('was-validated');
            window.alert(`Your order of ${this.props.label} has been processed`);
        }
    }

    render() {
        return (
            <form className={"payment-form"} noValidate>
                <div className={"form-container"}>
                    <div className={"form-row"}>
                        <div className={"form-group col-md-8"}>
                            <label className={"col-form-label col-form-label-sm text-muted"}>
                                Card Number
                                <div className={"input-group"}>
                                    <div className={"input-group-prepend card-icon"}>
                                        <div className={"input-group-text"}>
                                            <i className={"fas fa-credit-card"}></i>
                                        </div>
                                    </div>
                                    <input type={"text"} className={"form-control form-control-lg card-number"} placeholder={"xxxx xxxx xxxx xxxx"} pattern={"^([0-9]{4}\\s){3}[0-9]{4}$"} required />
                                    <div className={"input-group-append visa-icon"}>
                                        <div className={"input-group-text"}>
                                            <i className={"fab fa-cc-visa"}></i>
                                        </div>
                                    </div>
                                </div>
                            </label>
                        </div>
                        <div className={"form-group col"}>
                            <label className={"col-form-label col-form-label-sm text-muted"}>
                                CCV
                                <div className={"input-group"}>
                                    <input type={"text"} className={"form-control form-control-lg card-ccv"} placeholder={"xxx"} pattern={"^\\d\\d\\d$"} required />
                                    <div className={"input-group-append card-icon"}>
                                        <div className={"input-group-text"}>
                                            <i className={"fas fa-credit-card"}></i>
                                        </div>
                                    </div>
                                </div>
                            </label>
                        </div>
                    </div>
                    <div className={"form-row"}>
                        <div className={"form-group col-md-8"}>
                            <label className={"col-form-label col-form-label-sm text-muted"}>
                                Name on card
                                <input type={"text"} className={"form-control form-control-lg card-name"} required />
                            </label>
                        </div>
                        <div className={"form-group col"}>
                            <label className={"col-form-label col-form-label-sm text-muted"}>
                                MM
                                <input type={"text"} className={"form-control form-control-lg card-mm"} placeholder={"xx"} pattern={"^\\d\\d$"} required />
                            </label>
                        </div>
                        <div className={"form-group col"}>
                            <label className={"col-form-label col-form-label-sm text-muted"}>
                                YY
                                <input type={"text"} className={"form-control form-control-lg card-yy"} placeholder={"xx"} pattern={"^\\d\\d$"} required />
                            </label>
                        </div>
                    </div>
                </div>
                <div className={"SplitPane-btn-container"}>
                    <input type={"button"} className={"btn SplitPane-btn"} defaultValue={"Place Order"} onClick={this.handleSubmit.bind(this)} />
                </div>
            </form>
        )
    }
}

export default Payment;