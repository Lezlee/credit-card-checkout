import React from 'react';
import Payment from './payment';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';

beforeAll(() => {
    window.DOMTokenList = jest.fn().mockImplementation(() => {
        return {
            list: [],
            remove: jest.fn().mockImplementation(function (item) {
                const idx = this.list.indexOf(item);

                if (idx > -1) {
                    this.list.splice(idx, 1)
                }
            }),
            add: jest.fn().mockImplementation(function (item) {
                this.list.push(item);
            }),
            contains: jest.fn().mockImplementation(function (item) {
                return this.list.indexOf(item) > -1;
            })
        };
    });
})

describe('Payment Component', () => {
    test('should render correctly', () => {
        const wrapper = shallow(<Payment />);

        expect(shallowToJson(wrapper)).toMatchSnapshot();
    });

    test('should handle form validation appropriately', () => {
        const data = require('../../static/data/plans')
        const wrapper = shallow(<Payment plan={data[0]} />);
        const submitBtn = wrapper.find('input[type="button"]');
        let mockClassList = new DOMTokenList();

        submitBtn.simulate('click', {
            currentTarget: {
                form: {
                    checkValidity: () => false,
                    classList: mockClassList
                }
            },
            preventDefault: jest.fn(),
            stopPropagation: jest.fn()
        });

        expect(mockClassList.contains('was-validated')).toBeTruthy();

        submitBtn.simulate('click', {
            currentTarget: {
                form: {
                    checkValidity: () => true,
                    classList: mockClassList
                }
            },
            preventDefault: jest.fn(),
            stopPropagation: jest.fn()
        });

        expect(mockClassList.contains('was-validated')).toBeFalsy();
    });
});
