import React, { Component } from 'react';
import './plans.scss';
import plans from '../../static/data/plans';

export default class Plans extends Component {
    changePlan(e) {
        this.props.onChange(e.target.value)
    }

    render() {
        let { plan } = this.props
        let list = plans.map(currPlan => {
            return (
                <li className={"plans-main-list"} key={currPlan.type}>
                    <label>
                        <input name={"plans"}
                            type={"radio"}
                            value={currPlan.type}
                            checked={currPlan.type === plan}
                            onChange={this.changePlan.bind(this)} />
                        {` ${currPlan.label} (${currPlan.cost} /mo)`}
                        <ul>
                            {currPlan.details.map((detail, i) => {
                                return (<li key={i}>{detail}</li>)
                            })}
                        </ul>
                    </label>
                </li>
            )
        })
        return (
            <form className={"plans-form"} onSubmit={this.handleSubmit} noValidate>
                <ul>{list}</ul>
                <div className={"SplitPane-btn-container"}>
                    <button type="button"
                        className="btn SplitPane-btn"
                        onClick={this.props.onCheckout}>Checkout</button>
                </div>
            </form>
        )
    }
}
