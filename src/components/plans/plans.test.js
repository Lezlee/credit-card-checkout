import React from 'react';
import Plans from './plans';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';


describe('Plans Component', () => {
    test('should render correctly', () => {
        const data = require('../../static/data/plans')
        const changeCallback = jest.fn();
        const checkoutCallback = jest.fn();
        const plans = shallow(<Plans
            onChange={changeCallback}
            plans={data}
            plan={data[0]}
            onCheckout={checkoutCallback} />);

        expect(shallowToJson(plans)).toMatchSnapshot();
    });

    test('should call the onCheckout callback prop', () => {
        const data = require('../../static/data/plans')
        const changeCallback = jest.fn();
        const checkoutCallback = jest.fn();
        const plans = shallow(<Plans
            onChange={changeCallback}
            plans={data}
            plan={data[0]}
            onCheckout={checkoutCallback} />);

        plans.find('button').simulate('click');
        expect(checkoutCallback.mock.calls.length).toBe(1)
    })

    test('should call the onChange callback prop', () => {
        const data = require('../../static/data/plans');
        const changeCallback = jest.fn();
        const checkoutCallback = jest.fn();
        const plans = shallow(<Plans
            onChange={changeCallback}
            plans={data}
            plan={data[0]}
            onCheckout={checkoutCallback} />);
        const inputs = plans.first('ul').find('input[type="radio"]');

        inputs.at(inputs.length - 1).simulate('change', { target: { checked: true } });
        expect(changeCallback.mock.calls.length).toBe(1)
    })

});

