import React from 'react'
import './selected-plan.scss'
import plans from '../../static/data/plans';

export default class SelectedPlan extends React.Component {
    render() {
        const { plan } = this.props;
        // Get all the properties for the current plan
        const selectedPlan = plans.find((next) => {
            return next.type === plan;
        })
        // Convert plan details into a set of list items
        const planDetails = selectedPlan.details.map((detail, index) => {
            return (
                <li key={index}>
                    <span className={'fa-li'}>
                        <i className={'fas fa-check-circle'}></i>
                    </span>
                    {detail}
                </li>
            )
        })

        return (
            <div className={'plan'}>
                <div className={"plan-info"}>
                    <h3 className={'plan-title'}>{selectedPlan.label}</h3>
                    <hr />
                    <p className={'plan-cost'}>{`$${selectedPlan.cost}`}
                        <span>/mo</span>
                    </p>
                    <ul className={'plan-details fa-ul'}>
                        {planDetails}
                    </ul>
                </div>
            </div>
        )
    }
}
