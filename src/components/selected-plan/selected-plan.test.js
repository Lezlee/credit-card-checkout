import React from 'react';
import SelectedPlan from './selected-plan';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';


describe('Selected Plan Component', () => {
    test('should render correctly', () => {
        const data = require('../../static/data/plans');
        const component = shallow(<SelectedPlan plans={data} plan={data[0].type} />);

        expect(shallowToJson(component)).toMatchSnapshot();
    });

    test('should update the selected plan', () => {
        const data = require('../../static/data/plans');
        const component = shallow(<SelectedPlan plans={data} plan={data[0].type} />);

        expect(component.find('h3').text()).toContain(data[0].label)

        // Change plan
        component.setProps({ plan: data[1].type });

        expect(component.find('h3').text()).toContain(data[1].label)
    });
})