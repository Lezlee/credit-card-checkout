import React, { Component } from 'react';
import './App.scss';

import { Header, Footer } from '../components/layout';
import Main from './main/main';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <div className="container">
          <Main />
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
