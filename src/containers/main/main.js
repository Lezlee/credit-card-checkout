import React, { Component } from 'react';
import SplitPane from '../split-pane/split-pane';
import plans from '../../static/data/plans';
import './main.scss'

export default class Main extends Component {
    constructor(props) {
        super(props);

        this.state = {
            plan: plans[0].type,
            checkout: false
        };

        this.changePlan = this.changePlan.bind(this);
        this.goToCheckout = this.goToCheckout.bind(this);
        this.goToPlans = this.goToPlans.bind(this);
    }

    changePlan(newPlan) {
        this.setState({
            plan: newPlan
        });
    }

    goToCheckout() {
        this.setState({
            checkout: true
        });
    }

    goToPlans() {
        this.setState({
            checkout: false
        });
    }

    render() {
        return (
            <div className={"main-container"}>
                <main className={'container'}>
                    <SplitPane
                        plan={this.state.plan}
                        checkout={this.state.checkout}
                        changePlan={this.changePlan}
                        goToCheckout={this.goToCheckout}
                        goToPlans={this.goToPlans}>
                    </SplitPane>
                </main>
            </div>
        )
    }
}
