import React from 'react';
import Main from './main';
import Payment from '../../components/payment/payment';
import Plans from '../../components/plans/plans';
import { shallow, mount } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';


describe('Main Component', () => {
    test('should render correctly', () => {
        const wrapper = shallow(<Main />);

        expect(shallowToJson(wrapper)).toMatchSnapshot();
    });

    test('should render the correct component', () => {
        const wrapper = mount(<Main />);
        wrapper.setState({ checkout: false })

        expect(wrapper.find(Plans)).toHaveLength(1);
        expect(wrapper.find(Payment)).toHaveLength(0);

        // Change checkout state
        wrapper.setState({ checkout: true })

        expect(wrapper.find(Payment)).toHaveLength(1);
        expect(wrapper.find(Plans)).toHaveLength(0);
    })
});