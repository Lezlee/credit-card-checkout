import React from 'react';
import PropTypes from 'prop-types';
import Payment from '../../components/payment/payment';
import Plans from '../../components/plans/plans';
import SelectedPlan from '../../components/selected-plan/selected-plan'
import data from '../../static/data/plans';
import './split-pane.scss';
import CONSTANTS from '../../static/data/constants';
import wavesImage from '../../static/images/react-app-waves.png';

export default function SplitPane(props) {
    // Label to display in feedback if payment form is validated
    const label = data.find((item) => item.type === props.plan).label;
    const paymentComponent = <Payment label={label} />;

    const plansComponent = <Plans plan={props.plan}
        onChange={props.changePlan}
        onCheckout={props.goToCheckout} />;

    // Display either the plan component or the payment component based on the checkout prop
    const [rightPaneContent, rightPaneLabel, enableBackNav] = (props.checkout)
        ? [paymentComponent, CONSTANTS.checkoutLabel, true]
        : [plansComponent, CONSTANTS.plansLabel, false];

    return (
        <div className="row SplitPane">
            <div className="col-md-12 col-lg-5 SplitPane-left">
                <section>
                    {enableBackNav &&
                        <span onClick={props.goToPlans}>
                            <i className={"fas fa-arrow-circle-left fa-4x"}></i>
                        </span>}
                    {<SelectedPlan plan={props.plan} />}
                </section>
                <div className={"img-container"}>
                    <img src={wavesImage} alt={""}></img>
                </div>
            </div>
            <div className="col-md-12 col-lg-7 SplitPane-right">
                <section>
                    <div className={"SplitPane-right-label-container"}>
                        <p>{rightPaneLabel}</p>
                    </div>
                    {rightPaneContent}
                </section>
            </div>
        </div>
    );
}


SplitPane.propTypes = {
    plan: PropTypes.string.isRequired,
    checkout: PropTypes.bool,
    goToPlans: PropTypes.func.isRequired,
    changePlan: PropTypes.func.isRequired,
    onChangePlan: PropTypes.func
};

SplitPane.defaultProps = {
    plan: data[0].type,
    checkout: false
};
