import React from 'react';
import SplitPane from './split-pane';
import Payment from '../../components/payment/payment';
import Plans from '../../components/plans/plans';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import data from '../../static/data/plans';


describe('Split Pane Component', () => {
    test('should render correctly', () => {
        const changePlanCallback = jest.fn();
        const goToPlansCallback = jest.fn();
        const wrapper = shallow(<SplitPane plan={data[0].type}
            changePlan={changePlanCallback}
            goToPlans={goToPlansCallback} />);

        expect(shallowToJson(wrapper)).toMatchSnapshot();
    });

    test('should render the back button when the Payment component is in view', () => {
        const changePlanCallback = jest.fn();
        const goToPlansCallback = jest.fn();
        const wrapper = shallow(<SplitPane plan={data[0].type}
            changePlan={changePlanCallback}
            goToPlans={goToPlansCallback} />);

        expect(wrapper.find('.fas')).toHaveLength(0);

        wrapper.setProps({ checkout: true });

        expect(wrapper.find('.fas')).toHaveLength(1);
    })

    test('should replace the Payment component with the Plans compoment', () => {
        const changePlanCallback = jest.fn();
        const goToPlansCallback = jest.fn();
        const wrapper = shallow(<SplitPane plan={data[0].type}
            changePlan={changePlanCallback}
            goToPlans={goToPlansCallback} />);

        expect(wrapper.find(Payment)).toHaveLength(0);
        expect(wrapper.find(Plans)).toHaveLength(1);

        wrapper.setProps({ checkout: true });

        expect(wrapper.find(Payment)).toHaveLength(1);
        expect(wrapper.find(Plans)).toHaveLength(0);
    })
})
